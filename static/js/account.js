$(document).ready(function(){
	var timer;
	function checkCurrentPassword(input) {
		if (input.value !== ''){
			$('#current-input-password').addClass('is-valid').removeClass('is-invalid');
			$('#current-input-password-help').removeClass('text-danger').addClass('text-success').text('');
		} else {
			$('#current-input-password').addClass('is-invalid').removeClass('is-valid');
			$('#current-input-password-help').removeClass('text-success').addClass('text-danger').text('Укажите пароль');
		}
		countErrors();
	}
	function checkPassword(input) {
		let re = new RegExp(input.dataset.regex);
		if(re.test(input.value)){
			$('#input-password').addClass('is-valid').removeClass('is-invalid');
			$('#password-help').removeClass('text-danger').addClass('text-success').text(input.dataset.goodHint);
		} else {
			$('#input-password').addClass('is-invalid').removeClass('is-valid');
			$('#password-help').removeClass('text-success').addClass('text-danger').text(input.dataset.hint);
		}
		countErrors();
	}

	function checkAgainPassword() {
		if($('#input-again-password').val() == $('#input-password').val()){
			$('#input-again-password').addClass('is-valid').removeClass('is-invalid');
			$('#again-password-help').removeClass('text-danger').addClass('text-success').text($('#input-again-password').data('good-hint'));
		} else {
			$('#input-again-password').addClass('is-invalid').removeClass('is-valid');
			$('#again-password-help').removeClass('text-success').addClass('text-danger').text($('#input-again-password').data('hint'));
		}
		countErrors();
	}

	function countErrors(){
		let emptyInputs = 0;
		$('#chpass-form #current-input-password, #chpass-form #input-password, #chpass-form #input-again-password')
			.each(function(k, v){
				if ($(v).val() == ''){
					emptyInputs++;
				}
			})
		if($('.is-invalid').length + emptyInputs > 0){
			$('#submit-register').prop('disabled', true).addClass('disable-cur');
		} else {
			$('#submit-register').prop('disabled', false).removeClass('disable-cur');
		}
	}

	$('#current-input-password').on('keyup', function(){
		var input = this;
		clearTimeout(timer);
		checkCurrentPassword(input);
	});

	$('#current-input-password').on('focusout', function(){
		var input = this;
		clearTimeout(timer);
		checkCurrentPassword(input);
	});

	$('#input-password').on('keyup', function(){
		var input = this;
		clearTimeout(timer);
		timer = setTimeout(function(){
			checkPassword(input);
			if($('#input-again-password').val() != ''){
				checkAgainPassword();
			}
		}, 1000);
	});

	$('#input-password').on('focusout', function(){
		var input = this;
		clearTimeout(timer);
		checkPassword(input);
		if($('#input-again-password').val() != ''){
			checkAgainPassword();
		}
	});


	$('#input-again-password').on('keyup', function(){
		var input = this;
		clearTimeout(timer);
		timer = setTimeout(function(){checkAgainPassword(input)}, 100);
	});

	$('#input-again-password').on('focusout', function(){
		var input = this;
		clearTimeout(timer);
		checkAgainPassword();
	});

	$('#chpass-form').on('submit', function(){
		let form = $(this);
		$.post('/data/account/change_password', form.serializeArray()).
		done(function(data){
			if (typeof(data) == 'string') {
				data = JSON.parse(data);
			}
			if(data.status == 'ok'){
				eModal.alert({
					title: 'Успех',
					message: "Пароль изменён."
				});
				$('#changePass').modal('hide');
			} else {
				eModal.alert({
					title: 'Ошибка смены пароля',
					message: data.error_rus
				});
			}
		});
		return false;
	})

	function checkEmail(input) {
		let re = new RegExp(input.dataset.regex);
		if(re.test(input.value)){
			$.get('/data/account/registration/email_free', 
				{email: $(input).val()}
			)
			.done(function(data){
				if (typeof(data) == 'string') {
					data = JSON.parse(data);
				}
				if (data.result == 0) {
					$('#chemail-email').addClass('is-invalid').removeClass('is-valid');
					$('#chemail-email-help').removeClass('text-success').addClass('text-danger').text(data.text_ru);
				} else {
					$('#chemail-email').addClass('is-valid').removeClass('is-invalid');
					$('#chemail-email-help').removeClass('text-danger').addClass('text-success').text(data.text_ru);
				}
				countErrorsEmail();
			});
		} else {
			$('#chemail-email').addClass('is-invalid').removeClass('is-valid');
			$('#chemail-email-help').removeClass('text-success').addClass('text-danger').text(input.dataset.hint);
			countErrorsEmail();
		}
	}

	function checkEmailPassword(input) {
		if (input.value !== ''){
			$('#chemail-password').addClass('is-valid').removeClass('is-invalid');
			$('#chemail-password-help').removeClass('text-danger').addClass('text-success').text('');
			countErrorsEmail();
		} else {
			$('#chemail-password').addClass('is-invalid').removeClass('is-valid');
			$('#chemail-password-help').removeClass('text-success').addClass('text-danger').text('Укажите пароль');
			countErrorsEmail();
		}
	}

	function countErrorsEmail(){
		let emptyInputs = 0;
		$('#chemail-email, #chemail-password')
			.each(function(k, v){
				if ($(v).val() == ''){
					emptyInputs++;
				}
			})
		if($('.is-invalid').length + emptyInputs > 0){
			$('#submit-chemail').prop('disabled', true).addClass('disable-cur');
		} else {
			$('#submit-chemail').prop('disabled', false).removeClass('disable-cur');
		}
	}

	$('#chemail-email').on('keyup', function(){
		var input = this;
		clearTimeout(timer);
		timer = setTimeout(function(){
			checkEmail(input);
		}, 1000);
	});

	$('#chemail-email').on('focusout', function(){
		var input = this;
		clearTimeout(timer);
		checkEmail(input);
	});

	$('#chemail-password').on('keyup', function(){
		var input = this;
		clearTimeout(timer);
		checkEmailPassword(input);
	});

	$('#chemail-password').on('focusout', function(){
		var input = this;
		clearTimeout(timer);
		checkEmailPassword(input);
	});

	$('#chemail-form').on('submit', function(){
		let form = $(this);
		$.post('/data/account/change_email', form.serializeArray()).
		done(function(data){
			if (typeof(data) == 'string') {
				data = JSON.parse(data);
			}
			if(data.status == 'ok'){
				eModal.alert({
					title: 'Успех',
					message: "Код подтверждения смены почты отправлен на " + data.email + "."
				});
				$('#changeEmail').modal('hide');
			} else {
				eModal.alert({
					title: 'Ошибка смены почты',
					message: data.error_rus
				});
			}
		});
		return false;
	});

	if (window.location.hash == '#changePassword' || (typeof(openChangePass) != 'undefined' && openChangePass)) {
		$('#changePass').modal('show');
	} else if (window.location.hash == '#changeEmail' || (typeof(openChangeEmail) != 'undefined' && openChangeEmail)) {
			$('#changeEmail').modal('show');
		}
	countErrors();
});