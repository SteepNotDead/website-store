$(document).ready(function(){
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})
	$('body').on('click', '.auth-dropdown-opener', function(){
		$('#auth-dropdown-button').dropdown('toggle');
		return false;
	});

	$('body').on('click', '#auth-dropdown form', function(event){
		event.preventDefault();
		event.stopPropagation();
	});

	//authorization spinner
	function makeSpinner(){
		let spinnerRow = $('<div/>').addClass('row').addClass('auth-loading');
		let spinnerCol = $('<div/>').addClass('col').addClass('my-auto');
		let spinner = $('<div/>').addClass('spinner').addClass('mx-auto');
		spinner.append($('<div/>').addClass('bounce1'));
		spinner.append($('<div/>').addClass('bounce2'));
		spinner.append($('<div/>').addClass('bounce3'));
		spinnerCol.append(spinner);
		spinnerRow.append(spinnerCol);
		return spinnerRow;
	}

	function loginByForm(form) {
		var spinner = makeSpinner();
		$(form).find('.login-error').text('');
		function removeSpinner(){
			spinner.remove();
		}
		form.parent().parent().prepend(spinner);
		$.post('/data/account/login', form.serializeArray())
		.done(function(data){
			if (typeof(data) == 'string') {
				data = JSON.parse(data);
			}
			removeSpinner();
			if(data.status == 'ok'){
				if (typeof(returnTo) != 'undefined') {
					$(location).attr('href',returnTo);
				} else {
					location.reload();
				}
			} else {
				let errorMessage
				if(typeof(data.error_rus) != 'undefined' && data.error_rus != ''){
					errorMessage = data.error_rus;
				} else if (typeof(data.error) != 'undefined' && data.error != '') {
					errorMessage = data.error
				} else {
					errorMessage = 'Неопознанная ошибка авторизации.';
				}
				a = data;
				if (typeof(data.error_code) != 'undefined' && data.error_code == 123){
					eModal.alert({
						message: 'Пожалуйста, проверьте почтовый ящик, указанный при регистрации (в том числе, папку &laquo;спам&raquo;), найдите там письмо с кодом подтверждения регистрации и введите его <a href="/account/registration/confirm">здесь</a> или перейдите по ссылке в письме.',
						title: 'Вы не подтвердили свой email'
					});
				}
				$(form).find('.login-error').text(errorMessage);
			}
		});
	}

	$('body').on('click', '.login-button', function() {
		loginByForm($(this).closest('form'));
	});

	$('.login-form [name="login"]').keypress(function(event){
		if(event.which == 13){
			$(this).closest('.login-form').find('[name="password"]').focus();
			e.preventDefault();
		}
	})

	$('.login-form [name="password"]').keypress(function(event){
		if(event.which == 13){
			loginByForm($(this).closest('form'));
			e.preventDefault();
		}
	})

	$('.login-form').submit(function(event){
		event.preventDefault();
		return false;
	});

	$('body').on('click', '.logout-button', function(){
		$('#auth-dropdown').prepend(makeSpinner());
		$.get('/data/account/logout')
		.done(function(){
			location.reload();
		});
	});

});