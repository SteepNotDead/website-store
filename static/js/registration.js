$(document).ready(function(){
	var timer;
	function checkLogin(input) {
		let re = new RegExp(input.dataset.regex);
		if(re.test(input.value)){
			$.get('/data/account/registration/login_free', 
				{login: $(input).val()}
			)
			.done(function(data){
				if (typeof(data) == 'string') {
					data = JSON.parse(data);
				}
				if (data.result == 0) {
					$('#input-login').addClass('is-invalid').removeClass('is-valid');
					$('#login-help').removeClass('text-success').addClass('text-danger').text(data.text_ru);
				} else {
					$('#input-login').addClass('is-valid').removeClass('is-invalid');
					$('#login-help').removeClass('text-danger').addClass('text-success').text(data.text_ru);
				}
				countErrors();
			});
		} else {
			$('#input-login').addClass('is-invalid').removeClass('is-valid');
			$('#login-help').removeClass('text-success').addClass('text-danger').text(input.dataset.hint);
			countErrors();
		}
	}

	function checkPassword(input) {
		let re = new RegExp(input.dataset.regex);
		if(re.test(input.value)){
			$('#input-password').addClass('is-valid').removeClass('is-invalid');
			$('#password-help').removeClass('text-danger').addClass('text-success').text(input.dataset.goodHint);
		} else {
			$('#input-password').addClass('is-invalid').removeClass('is-valid');
			$('#password-help').removeClass('text-success').addClass('text-danger').text(input.dataset.hint);
		}
		countErrors();
	}

	function checkAgainPassword() {
		if($('#input-again-password').val() == $('#input-password').val()){
			$('#input-again-password').addClass('is-valid').removeClass('is-invalid');
			$('#again-password-help').removeClass('text-danger').addClass('text-success').text($('#input-again-password').data('good-hint'));
		} else {
			$('#input-again-password').addClass('is-invalid').removeClass('is-valid');
			$('#again-password-help').removeClass('text-success').addClass('text-danger').text($('#input-again-password').data('hint'));
		}
		countErrors();
	}

	function checkEmail(input) {
		let re = new RegExp(input.dataset.regex);
		if(re.test(input.value)){
			$.get('/data/account/registration/email_free', 
				{email: $(input).val()}
			)
			.done(function(data){
				if (typeof(data) == 'string') {
					data = JSON.parse(data);
				}
				if (data.result == 0) {
					$('#input-email').addClass('is-invalid').removeClass('is-valid');
					$('#email-help').removeClass('text-success').addClass('text-danger').text(data.text_ru);
				} else {
					$('#input-email').addClass('is-valid').removeClass('is-invalid');
					$('#email-help').removeClass('text-danger').addClass('text-success').text(data.text_ru);
				}
				countErrors();
			});
		} else {
			$('#input-email').addClass('is-invalid').removeClass('is-valid');
			$('#email-help').removeClass('text-success').addClass('text-danger').text(input.dataset.hint);
			countErrors();
		}
	}

	function countErrors(){
		let emptyInputs = 0;
		$('#input-login, #input-password, #input-again-password, #input-email')
			.each(function(k, v){
				if ($(v).val() == ''){
					emptyInputs++;
				}
			})
		if($('.is-invalid').length + emptyInputs > 0){
			$('#submit-register').prop('disabled', true).addClass('disable-cur');
		} else {
			$('#submit-register').prop('disabled', false).removeClass('disable-cur');
		}
	}


	$('#input-login').on('keyup', function(){
		var input = this;
		clearTimeout(timer);
		timer = setTimeout(function(){checkLogin(input)}, 1000);
	});

	$('#input-login').on('focusout', function(){
		var input = this;
		clearTimeout(timer);
		checkLogin(input);
	});


	$('#input-password').on('keyup', function(){
		var input = this;
		clearTimeout(timer);
		timer = setTimeout(function(){
			checkPassword(input);
			if($('#input-again-password').val() != ''){
				checkAgainPassword();
			}
		}, 1000);
	});

	$('#input-password').on('focusout', function(){
		var input = this;
		clearTimeout(timer);
		checkPassword(input);
		if($('#input-again-password').val() != ''){
			checkAgainPassword();
		}
	});


	$('#input-again-password').on('keyup', function(){
		var input = this;
		clearTimeout(timer);
		timer = setTimeout(function(){checkAgainPassword(input)}, 100);
	});

	$('#input-again-password').on('focusout', function(){
		var input = this;
		clearTimeout(timer);
		checkAgainPassword();
	});

	$('#input-email').on('keyup', function(){
		var input = this;
		clearTimeout(timer);
		timer = setTimeout(function(){checkEmail(input)}, 1000);
	});

	$('#input-email').on('focusout', function(){
		var input = this;
		clearTimeout(timer);
		checkEmail(input);
	});

	if($('#input-login').val() != ''){
		checkLogin($('#input-login')[0]);
	}
	if($('#input-password').val() != ''){
		checkPassword($('#input-password')[0]);
		checkAgainPassword();
	}
	if($('#input-again-password').val() != ''){
		checkAgainPassword();
	}
	if($('#input-email').val() != ''){
		checkEmail($('#input-email')[0]);
	}

	$('#regform').on('submit', function(){
		var form = $(this);
		$.post('/data/account/registration/save', form.serializeArray())
		.done(function(data){
			if (typeof(data) == 'string') {
				data = JSON.parse(data);
			}
			if (data.status == 'error'){
				eModal.alert({
					message: data.error_rus,
					title: 'Ошибка регистрации'
				});
			} else {
				let container = form.parent();
				$('#regform').remove();
				container.html(
					$('<div/>')
					.append(
						$('<strong/>').text('Регистрация почти завершена.')
					)
					.append(
						$('<p>').html('Подтвердите указанный почтовый ящик, перейдя по ссылке из письма или введя код из письма <a href="/account/registration/confirm/">здесь</a>.<br>Если письмо не пришло, проверьте папку со спамом.<br>Поспешите, если в течение часа почтовый ящик не будет подтверждён, ваша регистрация будет аннулирована.')
					)
				);
			}
		})
		return false;
	})

	countErrors();
});